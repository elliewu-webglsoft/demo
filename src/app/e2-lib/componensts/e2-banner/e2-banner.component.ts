import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'e2-banner',
  templateUrl: './e2-banner.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class E2BannerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
