import { LayoutConfig } from '@spartacus/storefront';

export function pageLayoutConfig(): LayoutConfig {
  return {
    layoutSlots: {
      header: {
        lg: {
          slots: [
            'PreHeader', 'SiteLogo', 'SearchBox', 'MiniCart', 'NavigationBar',
          ],
        },
      },
      footer: {
        slots: ['Footer'],
      }
    }
  }
}
