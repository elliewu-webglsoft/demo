import { LayoutSlotConfig } from '@spartacus/storefront';
import { E2HeaderConfig } from './e2-header/e2-header.config';

export const E2HomepageConfig: LayoutSlotConfig = {
  e2HomepageTemplate: {
    ...E2HeaderConfig,
    slots: [],
  },
};
